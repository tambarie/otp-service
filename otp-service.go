package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	api "otp-service/internal/adapters/api/otp"
	"otp-service/internal/adapters/repository"
	"otp-service/internal/core/helper"
	services "otp-service/internal/core/services/otp"
	"otp-service/internal/core/shared"
	"otp-service/internal/ports"
	"time"
)

func main() {
	helper.InitializeLogDir()
	//mongoURL := "mongodb://localhost:27017"
	address, port, _, dbHost, dbName, _, _ := helper.LoadConfig()
	dbRepository := ConnectToMongo(dbHost, dbName)
	service := services.New(dbRepository)
	handler := api.NewHTTPHandler(service)

	router := gin.Default()
	router.Use(helper.LogRequest)

	router.POST("/:api-gateway/otp/requests", handler.CreateOTP)
	router.POST("/:api-gateway/otp/requests/validate", handler.ValidateOtp)

	router.NoRoute(func(ctx *gin.Context) {
		ctx.JSON(404,
			helper.PrintErrorMessage("404", shared.NoResourceFound))
	})

	fmt.Println("service running on " + address + ":" + port)
	helper.LogEvent("info", fmt.Sprintf("started otp service on "+address+":"+port+" in "+time.Since(time.Now()).String()))
	_ = router.Run(":" + port)
}

func ConnectToMongo(mongoURL string, dbName string) ports.OTPRepository {
	repo, err := repository.NewMongoRepository(mongoURL, dbName, 2000)
	if err != nil {
		_ = helper.PrintErrorMessage("500", err.Error())
		log.Fatal(err)
	}
	return services.New(repo)
}
