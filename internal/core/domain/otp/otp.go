package domain

import (
	"github.com/pquerna/otp"
	"io"
	"time"
)

type OTP struct {
	Reference       string    `json:"reference"`
	Code            string    `json:"code"`
	Strict          bool      `json:"strict"`
	CreatedOn       time.Time `json:"created_on"`
	ExpiresAT       time.Time `json:"expires_at"`
	DeviceReference string    `json:"device_reference"`
	UserReference   string    `json:"user_reference"`
	Secret          string
}

// GenerateOPTs provides options for .Generate()

type GenerateOPTS struct {
	Issuer      string
	AccountName string
	SecretSize  uint
	Secret      []byte
	Digits      otp.Digits
	Algorithm   otp.Algorithm
	Rand        io.Reader
}

type ValidateOPTS struct {
	Digits    otp.Digits
	Algorithm otp.Algorithm
}
