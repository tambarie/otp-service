package services

import (
	domain "otp-service/internal/core/domain/otp"
	"otp-service/internal/ports"
)

type Service struct {
	otpRepository ports.OTPRepository
}

func New(otpRepository ports.OTPRepository) *Service {
	return &Service{otpRepository: otpRepository}
}

func (s *Service) ValidateOTP(userReference string) ([]*domain.OTP, bool, error) {
	return s.otpRepository.ValidateOTP(userReference)
}

func (s *Service) SaveOTP(otp *domain.OTP) (*domain.OTP, error) {
	return s.otpRepository.SaveOTP(otp)
}
