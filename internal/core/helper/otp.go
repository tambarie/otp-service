package helper

import (
	"crypto/hmac"
	"crypto/subtle"
	"encoding/base32"
	"encoding/binary"
	"fmt"
	"github.com/pquerna/otp"
	"math"
	domain "otp-service/internal/core/domain/otp"
	"strings"
)

const debug = false

func ValidateOTP(passcode string, counter uint64, secret string) bool {
	rv, _ := ValidateCustom(
		passcode,
		counter,
		secret,
		domain.ValidateOPTS{
			Digits:    otp.DigitsSix,
			Algorithm: otp.AlgorithmSHA1,
		},
	)
	return rv
}

func GenerateCode(secret string, counter uint64) (string, error) {
	return GenerateCodeCustom(secret, counter, domain.ValidateOPTS{
		Digits:    otp.DigitsSix,
		Algorithm: otp.AlgorithmSHA1,
	})
}

func ValidateCustom(passcode string, counter uint64, secret string, otps domain.ValidateOPTS) (bool, error) {
	passcode = strings.TrimSpace(passcode)

	if len(passcode) != otps.Digits.Length() {
		return false, otp.ErrValidateInputInvalidLength
	}
	otpstr, err := GenerateCodeCustom(secret, counter, otps)
	if err != nil {
		return false, err
	}
	if subtle.ConstantTimeCompare([]byte(otpstr), []byte(passcode)) == 1 {
		return true, nil
	}
	return false, nil
}

func GenerateCodeCustom(secret string, counter uint64, otps domain.ValidateOPTS) (passcode string, err error) {
	secret = strings.TrimSpace(secret)
	if n := len(secret) % 8; n != 0 {
		secret = secret + strings.Repeat("=", 8-n)
	}

	secret = strings.ToUpper(secret)

	secretBytes, err := base32.StdEncoding.DecodeString(secret)
	if err != nil {
		return "", otp.ErrValidateSecretInvalidBase32
	}

	buf := make([]byte, 8)
	mac := hmac.New(otps.Algorithm.Hash, secretBytes)
	binary.BigEndian.PutUint64(buf, counter)

	if debug {
		fmt.Printf("counter=%v\n", counter)
		fmt.Printf("counter=%v\n", counter)
	}
	mac.Write(buf)
	sum := mac.Sum(nil)

	offset := sum[len(sum)-1] & 0xf
	value := int64(((int(sum[offset]) & 0x7f) << 24) | ((int(sum[offset+1] & 0xff)) << 16) | ((int(sum[offset+2] & 0xff)) << 8) | (int(sum[offset+3]) & 0xff))

	l := otps.Digits.Length()
	mod := int32(value % int64(math.Pow10(l)))

	if debug {
		fmt.Printf("offset=%v\n", offset)
		fmt.Printf("value=%v\n", value)
		fmt.Printf("value=%v\n", mod)
	}

	return otps.Digits.Format(mod), nil
}

// Generate To be used in case am to pass the data as URL Query strings

//var b32NoPadding = base32.StdEncoding.WithPadding(base32.NoPadding)
//func Generate(otps *domain.GenerateOPTS) (*otp.Key, error)  {
//	if otps.Issuer == ""{
//		return nil, otp.ErrGenerateMissingIssuer
//	}
//	if otps.AccountName == ""{
//		return nil,otp.ErrGenerateMissingAccountName
//	}
//	if otps.SecretSize == 0 {
//		otps.SecretSize = 10
//	}
//	if otps.Digits == 0 {
//		otps.Digits = otp.DigitsSix
//	}
//	if otps.Rand == nil{
//		otps.Rand = rand.Reader
//	}
//
//	v :=url.Values{}
//	if len(otps.Secret) != 0 {
//		v.Set("secret",b32NoPadding.EncodeToString(otps.Secret))
//	} else {
//		secret := make([]byte, otps.SecretSize)
//		_, err := otps.Rand.Read(secret)
//		if err != nil{
//			return nil, err
//		}
//		v.Set("secret",b32NoPadding.EncodeToString(secret))
//	}
//	v.Set("issuer", otps.Issuer)
//	v.Set("algorithm",otps.Algorithm.String())
//	v.Set("digits",otps.Digits.String())
//
//	u := url.URL{
//		Scheme:      "otpauth",
//		Host:        "hotp",
//		Path:        "/" + otps.Issuer + ":" + otps.AccountName,
//		RawQuery:    v.Encode(),
//
//	}
//	return otp.NewKeyFromURL(u.String())
//}
