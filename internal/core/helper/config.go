package helper

import (
	"flag"
	"github.com/spf13/viper"
	"log"
	"os"
)

type ConfigStruct struct {
	Address            string `mapstructure:"address"`
	Port               string `mapstructure:"port"`
	Mode               string `mapstructure:"mode"`
	DBHost             string `mapstructure:"db_host"`
	DBName             string `mapstructure:"db_name"`
	MongoURL           string `mapstructure:"mongoURL"`
	AppName            string `mapstructure:"app_name"`
	LogDir             string `mapstructure:"log_dir"`
	LogFile            string `mapstructure:"log_file"`
	ExternalConfigPath string `mapstructure:"external_config_path"`
}

var (
	address            string
	port               string
	mode               string
	dbHost             string
	dbName             string
	mongoURL           string
	externalConfigPath string
)

func LoadConfig() (string, string, string, string, string, string, string) {
	flag.StringVar(&address, "address", Config.Address, "local host")
	flag.StringVar(&port, "port", Config.Port, "application ports")
	flag.StringVar(&mode, "mode", Config.Mode, "application mode, either dev or production")
	flag.StringVar(&dbHost, "dbhost", Config.DBHost, "database host")
	flag.StringVar(&dbName, "dbname", Config.DBName, "database name")
	flag.StringVar(&mongoURL, "mongoURL", Config.MongoURL, "database URL")
	flag.StringVar(&externalConfigPath, "external_config_path", Config.DBName, "external config path")

	flag.Parse()
	for i, value := range flag.Args() {
		os.Args[i] = value
	}
	return address, port, mode, dbHost, dbName, externalConfigPath, mongoURL
}

func LoadEnv(path string) (config ConfigStruct, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigName("otp-service")
	viper.SetConfigType("env")

	viper.AutomaticEnv()
	err = viper.ReadInConfig()
	if err != nil {
		return ConfigStruct{}, err
	}
	err = viper.Unmarshal(&config)
	return
}

func ReturnConfig() ConfigStruct {
	config, err := LoadEnv(".")
	if err != nil {
		log.Println(err)
	}
	if config.ExternalConfigPath != "" {
		viper.Reset()
		config, err = LoadEnv(config.ExternalConfigPath)
		if err != nil {
			log.Println(err)
		}
	}
	return config
}

var Config = ReturnConfig()
