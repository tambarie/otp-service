package repository

import (
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/net/context"
	"log"
	domain "otp-service/internal/core/domain/otp"
	"time"
)

type MongoRepository struct {
	Client  *mongo.Client
	Db      string
	Timeout time.Duration

}

func (m *MongoRepository) SaveOTP(otp *domain.OTP) (*domain.OTP, error) {
	coll := m.Client.Database("otp").Collection("otp-service")
	result, err := coll.InsertOne(context.TODO(), otp)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Inserted document with _id: %v\n", result.InsertedID)
	return otp, nil
}

func (m *MongoRepository) ValidateOTP(userReference string) ([]*domain.OTP, bool, error) {
	coll := m.Client.Database("otp").Collection("otp-service")
	filter := bson.D{{"reference", userReference}}
	cursor, err := coll.Find(context.TODO(), filter)
	if err != nil {
		panic(err)
	}
	var results []*domain.OTP
	if err = cursor.All(context.TODO(), &results); err != nil {
		log.Fatal(err)
	}
	return results, true, err
}
