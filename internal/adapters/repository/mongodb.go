package repository

import (
	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/net/context"
	"otp-service/internal/ports"
	"time"
)

func newMongoClient(mongoURL string, mongoTimeout int) (*mongo.Client, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(mongoTimeout))
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(mongoURL))
	if err != nil {
		return nil, err
	}
	return client, nil
}

func NewRedisClient (redisHost int,redisPort int)  {

}
func NewMongoRepository(mongoURL, mongoDB string, mongoTimeout int) (ports.OTPRepository, error) {
	repo := &MongoRepository{
		Db:      mongoDB,
		Timeout: time.Duration(mongoTimeout) * time.Second,
	}
	client, err := newMongoClient(mongoURL, mongoTimeout)
	if err != nil {
		return nil, errors.Wrap(err, "client error")
	}
	repo.Client = client
	return repo, nil
}
