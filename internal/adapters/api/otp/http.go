package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
	domain "otp-service/internal/core/domain/otp"
	"otp-service/internal/core/shared"
	"time"

	"log"

	"otp-service/internal/core/helper"

	"otp-service/internal/ports"
)

type HTTPHandler struct {
	otpService ports.OTPService
}

func NewHTTPHandler(otpService ports.OTPService) *HTTPHandler {
	return &HTTPHandler{
		otpService: otpService,
	}
}

// CreateOTP Handler
func (h *HTTPHandler) CreateOTP(ctx *gin.Context) {
	otp := &domain.OTP{}
	secret, err := helper.GenerateRandomString(8)
	if err != nil {
		log.Println(err)
	}
	var counter uint64 = 8

	// Bind JSON
	err = ctx.BindJSON(otp)
	if err != nil {
		log.Fatalf("unable to bind JSON %v", err)
	}

	if otp.Strict == false {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": shared.STRICT_VALIDATION_ERROR,
		})
		return
	}

	// Check in the database if user reference exists in the database
	if otp.UserReference == "" {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": shared.AUTHORIZATION_ERROR,
		})
		return
	}

	otpCode, err := helper.GenerateCode(secret, counter)
	if err != nil {
		fmt.Println(err)
		return
	}

	otp.Code = otpCode
	otp.Reference = uuid.New().String()
	otp.CreatedOn = time.Now()
	otp.ExpiresAT = time.Now().Add(20 * time.Minute)
	otp.DeviceReference = uuid.New().String()
	otp.Secret = secret

	// will handle saving the struct to database

	db, err := h.otpService.SaveOTP(otp)
	if err != nil {
		ctx.AbortWithStatusJSON(400, err)
		return
	}

	// Json response
	ctx.JSON(201, gin.H{
		"reference": db.Reference,
		"code":      db.Code,
	})
}

// ValidateOTP Handler

func (h *HTTPHandler) ValidateOtp(ctx *gin.Context) {

	otp := &domain.OTP{}
	currentTime := time.Now()

	err := ctx.BindJSON(otp)
	if err != nil {
		log.Fatalf("unable to bind JSON %v", err)
		return
	}

	fmt.Println(otp.Reference)
	response, _, err := h.otpService.ValidateOTP(otp.Reference)

	var counter uint64 = 8
	var secret string
	var dueTime time.Time
	for _, v := range response {
		dueTime = v.ExpiresAT
		secret = v.Secret
	}

	if err != nil {
		log.Fatalf("unable to get user reference %v", err)
		return
	}

	timeDuration := currentTime.Sub(dueTime)
	var fifteenMinutes time.Duration
	fifteenMinutes = 15 * time.Minute

	if timeDuration > fifteenMinutes {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": shared.EXPIRED_OTP_CODE_ERROR,
		})
		return
	}

	//Validator
	isValid := helper.ValidateOTP(otp.Code, counter, secret)
	if isValid != true {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": shared.STRICT_VALIDATION_ERROR,
		})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{
		"is_valid": isValid,
	})
}
