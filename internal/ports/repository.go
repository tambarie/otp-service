package ports

import domain "otp-service/internal/core/domain/otp"

type OTPRepository interface {
	SaveOTP(otp *domain.OTP) (*domain.OTP, error)
	ValidateOTP(userReference string) ([]*domain.OTP, bool, error)
}
